﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace replacer {
    class Program {
        protected Program() { }

        private static readonly string READING_DICTIONARY_FILE = "Reading dictionary file ";
        private static readonly string VALIDATING_DICTIONARY_FILE = "Validating dictionary file ";
        private static readonly string READING_TEXT_FILE = "Reading text file ";

        private static readonly string REGEX_PATTERN = @"(\""([^\""]*)\""|(?:[^;]*))";

        private static readonly int MAX_PAD_RIGHT = 50;

        static void Main() {

            string baseFolder = System.Windows.Forms.Application.StartupPath;

            List<string> errors = new List<string>();

            try {
                Console.WriteLine();
                Console.Write($"Initializing config from {Config.FILE_NAME} ...".PadRight(MAX_PAD_RIGHT));

                JsonSerializerSettings jsonSettings = new JsonSerializerSettings {
                    DefaultValueHandling = DefaultValueHandling.Ignore
                };

                Config config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(Config.FILE_NAME), jsonSettings);

                Decomposer decomposer = new Decomposer(REGEX_PATTERN);

                Inspector inspector = new Inspector(2, decomposer);

                WriteSuccess($"Done");

                Console.WriteLine();
                Console.Write($"{READING_DICTIONARY_FILE}{config.DictionaryFileName} ...".PadRight(MAX_PAD_RIGHT));

                IDictionary<string, string> dictionary = new Dictionary<string, string>();

                IList<string> lines = File.ReadAllLines(config.DictionaryFileName, new UTF8Encoding(false));
                string dictionaryFileText = File.ReadAllText(config.DictionaryFileName, new UTF8Encoding(false));

                WriteSuccess($"Done");

                Console.WriteLine();
                Console.Write($"{VALIDATING_DICTIONARY_FILE}{config.DictionaryFileName} ...".PadRight(MAX_PAD_RIGHT));

                string[] validationErrors = inspector.GetValidationErrors(dictionaryFileText);

                if (validationErrors.Any()) {
                    throw new InvalidOperationException(string.Join(Environment.NewLine, validationErrors));
                }

                WriteSuccess($"Done");

                foreach (string line in lines) {
                    string[] matchtes = decomposer.Split(line);

                    string key = matchtes.First();
                    string value = matchtes.Last();

                    if (string.IsNullOrWhiteSpace(key)) {
                        errors.Add($"Ignoring {line}. Reason: Key is empty");
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(value)) {
                        errors.Add($"Ignoring {line}. Reason: Value is empty");
                        continue;
                    }


                    if (dictionary.ContainsKey(key)) {
                        errors.Add($"Dictionary already contains {key}.");
                        continue;
                    }

                    dictionary.Add(key, value);
                }


                KeyValuePair<int, int> maxLength = new KeyValuePair<int, int>(
                    dictionary.Keys.Select(key => key.Length).OrderBy(length => length).Max(),
                    dictionary.Values.Select(value => value.Length).OrderBy(length => length).Max());

                string sourceFolderName = baseFolder + config.SourceFolderName;
                string resultFolderName = baseFolder + config.ResultFolderName;

                string[] fullFileNames = Directory.GetFiles(sourceFolderName);

                if (!fullFileNames.Any()) {
                    throw new InvalidOperationException($"{sourceFolderName} is empty");
                }

                Console.WriteLine($"{Environment.NewLine}{Environment.NewLine}Processing {fullFileNames.Length} files:");

                foreach (string fileName in fullFileNames.Select(fullFileName => Path.GetFileName(fullFileName))) {
                    Console.Write($"  {fileName}");
                    string text = File.ReadAllText(sourceFolderName + fileName, new UTF8Encoding(false));
                    foreach (KeyValuePair<string, string> entry in dictionary) {
                        text = text.Replace(entry.Key, entry.Value);
                    }
                    File.WriteAllText(resultFolderName + fileName, text, new UTF8Encoding(false));
                    WriteSuccess($"\t\tDone{Environment.NewLine}");
                }
            } catch (Exception e) {
                Console.WriteLine();
                WriteErrorLine(e.Message);
            }

            if (errors.Any()) {
                Console.WriteLine();
                WriteErrorLine("The following errors ocured");
                errors.ForEach(error => WriteErrorLine(error));
            }

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Press any key to exit ... ");
            Console.ReadKey();
        }

        static void WriteErrorLine(string message) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static void WriteSuccess(string message) {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}

