﻿using Xunit;

namespace replacer.UnitTests {
    public class DecomposerTest {

        private static readonly string REGEX_PATTERN = @"(\""([^\""]*)\""|(?:[^;]*))";
        private static readonly int EXPECTED_MATCHES_COUNT = 2;

        private readonly Decomposer decomposer = new Decomposer(REGEX_PATTERN);

        [Fact]
        public void NoQuotesSplit() {

            string text = "foo;bar";

            string[] matches = this.decomposer.Split(text);

            Assert.Equal(EXPECTED_MATCHES_COUNT, matches.Length);
        }
    }
}
