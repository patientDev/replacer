﻿using System;
using System.Collections.Generic;
using System.IO;

namespace replacer {
    class Inspector {

        private readonly uint expectedMatchesCount;
        private readonly Decomposer decomposer;

        public Inspector(uint expectedMatchesCount, Decomposer decomposer) {
            this.expectedMatchesCount = expectedMatchesCount;
            this.decomposer = decomposer;
        }

        public string[] GetValidationErrors(string text) {
            List<string> errors = new List<string>();
            using (StringReader reader = new StringReader(text)) {
                while (true) {
                    string line = reader.ReadLine();
                    if (string.IsNullOrWhiteSpace(line)) {
                        break;
                    }
                    try {
                        string[] matches = this.decomposer.Split(line);
                        if (matches.Length != this.expectedMatchesCount) {
                            errors.Add($"Line {line} has not expected matches count {this.expectedMatchesCount}.");
                        }
                    } catch (Exception e) {
                        errors.Add(e.Message);
                    }
                }
            }
            return errors.ToArray();
        }
    }
}
