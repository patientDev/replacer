﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace replacer {
    public class Decomposer {

        private static readonly string QUOTATION_MARK = "\"";
        private static readonly string ESCAPED_QUOTATION_MARK = "\\\"";

        private readonly string splitRegexPattern;

        public Decomposer(string splitRegexPattern) {
            this.splitRegexPattern = splitRegexPattern;
        }

        public string[] Split(string text) {
            List<string> matches = new List<string>();
            foreach (Match match in Regex.Matches(text, this.splitRegexPattern, RegexOptions.Multiline)) {
                if (!string.IsNullOrWhiteSpace(match.Value)) {
                    string value = match.Value;
                    if (value.StartsWith(QUOTATION_MARK, StringComparison.InvariantCulture)) {
                        value = value.TrimStart(QUOTATION_MARK.ToCharArray()).TrimEnd(QUOTATION_MARK.ToCharArray());
                    }
                    value = value.Replace(ESCAPED_QUOTATION_MARK, QUOTATION_MARK);
                    matches.Add(value);
                }
            }
            return matches.ToArray();
        }
    }
}
