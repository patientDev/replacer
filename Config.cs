﻿using Newtonsoft.Json;

namespace replacer {
    class Config {
        public static string FILE_NAME => "config.json";

        [JsonProperty("dictionaryFileName")]
        public string DictionaryFileName { get; private set; }

        [JsonProperty("sourceFolderName")]
        public string SourceFolderName { get; private set; }

        [JsonProperty("resultFolderName")]
        public string ResultFolderName { get; private set; }
    }
}
